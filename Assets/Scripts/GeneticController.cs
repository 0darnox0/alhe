﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class GeneticController : MonoBehaviour
{
    public GameObject carPrefab;

    public int numberOfCars = 30;
    public int[] hiddenLayersSizes = {32, 32};
    public int numberOfRays = 7;
    public double crossoverProbability = 0.95;
    public double mutationProbability = 0.01;
    public bool elitism = true;

    public int generationNumber { get; private set; } = 0;

    private int inputLayerSize;
    private int outputLayerSize;
    private int[] layerSizes;
    private int numberOfGenes;

    private List<GameObject> activeCars;
    private List<GameObject> population;
    private GeneticAlgorithm geneticAlgorithm;
    public int stopAfterGeneration = 50;

    private int runningCars = 0;
    public double distanceFactor = 1.0f;
    public double speedFactor = 0.0f;

    public GameObject bestCar { get; private set; }

    private Color carGlowColor = new Color(0, 251.1946f, 515.8943f);
    private Color carFillColor = new Color(0, 0.1030961f, 0.5660378f);
    private Color bestCarGlowColor = new Color(515.8943f, 135.9131f, 0f);
    private Color bestCarFillColor = new Color(0.5283019f, 0.2556967f, 0.03737985f);

    public Text simulationInfo;
    public double TopDistance { get; private set; } = 0;
    public double MaxSpeed { get; private set; } = 0;
    public double MaxFitness { get; private set; } = 0;

    public enum SelectionType {UBest, RouletteWheel, Tournament}
   [Header("Selection Settings")]
   public  SelectionType selectionType;
   public int tournamentSize = 8;
   public int uSize = 8;
   
   public enum CrossoverType {KPoint, SinglePoint, UniformPoint, FixedPointByNeurons}
   [Header("Crossover Settings")]
   public  CrossoverType crossoverType;
   public int numberOfDivisionPoints = 5;

   public enum MutationType {Uniform, Shrink}
   [Header("Mutation Settings")]
   public MutationType mutationType;
   public int shrinkMutationRate = 5;
   public int uniformMutationRate = 5;

   public GameObject cameraFollowerObject;
   private CameraFollower cameraFollower;
   public bool simulationFinished { get; private set; }
   public double limitDistance = 1500;

   private Queue<GameObject> carsPool;
   private Dictionary<GameObject, CarAIControl> carsAIControls;
   private Dictionary<GameObject, Renderer> carsBodyRenders;
   
    void Awake()
    {
        cameraFollower = cameraFollowerObject.GetComponent<CameraFollower>();
        carsPool = new Queue<GameObject>(numberOfCars);
        carsAIControls = new Dictionary<GameObject, CarAIControl>();
        carsBodyRenders = new Dictionary<GameObject, Renderer>();
        
        activeCars = new List<GameObject>();
        population = new List<GameObject>();
    }
    
    private void OnEnable()
    {
        ResetController();
        StartCoroutine(AlgorithmLoop());
    }

    private void ResetController()
    {
        simulationFinished = false;
        generationNumber = 0;
        TopDistance = 0;
        MaxSpeed = 0;
        MaxFitness = 0;

        InstantiateCars();
        
        inputLayerSize = numberOfRays;
        outputLayerSize = 2; // acceleration + steering
        SetLayerSizes();

        numberOfGenes = CalculateNumberOfParameters(layerSizes);
        geneticAlgorithm = GetGeneticAlgorithm();
    }

    private void InstantiateCars()
    {
        var remainingCars = Math.Max(0, numberOfCars - carsPool.Count);
        for (int i = 0; i < remainingCars; i++)
        {
            var car = Instantiate(carPrefab);
            car.SetActive(false);
            carsPool.Enqueue(car);

            var carAIControl = car.GetComponent<CarAIControl>();
            carsAIControls.Add(car, carAIControl);
            
            var bodyRenderer = car.transform.Find("SkyCar/SkyCarBody").GetComponent<Renderer>();
            carsBodyRenders.Add(car, bodyRenderer);
        }
    }

    void FixedUpdate()
    {
        UpdateBestCar();
        UpdateStatistics();
        SetDisplayInfo();
        StopGoodEnoughCars();
    }

    private void StopGoodEnoughCars()
    {
        if (!bestCar) return;
        
        var bestDistance = carsAIControls[bestCar].totalDistance;
        if (bestDistance >= limitDistance)
        {
            foreach (var car in activeCars.ToList())
            {
                carsAIControls[car].TriggerCarStop();
            }

            simulationFinished = true;
        }
    }

    private void UpdateBestCar()
    {
        var newBestCar = BestCar();

        if (newBestCar != bestCar)
        {
            ColorBestCar(newBestCar);
            UpdateCameraTarget(newBestCar);
        }

        bestCar = newBestCar;
    }

    private void UpdateCameraTarget(GameObject newBestCar)
    {
        if(!newBestCar) return;
        
        cameraFollower.SetTarget(newBestCar.transform);
    }

    private void ColorBestCar(GameObject newBestCar)
    {
        if (newBestCar == bestCar) return;

        ColorCar(bestCar, carGlowColor, carFillColor);
        ColorCar(newBestCar, bestCarGlowColor, bestCarFillColor);
    }

    private void ColorCar(GameObject car, Color glowColor, Color fillColor)
    {
        if (!car) return;

        var bodyRenderer = carsBodyRenders[car];
        
        bodyRenderer.material.SetColor("Color_A8B1B411", glowColor);
        bodyRenderer.material.SetColor("Color_5F994916", fillColor);
    }

    private IEnumerator AlgorithmLoop()
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        while (generationNumber < stopAfterGeneration && !simulationFinished)
        {
            generationNumber++;

            var populationGenes = geneticAlgorithm.GetPopulation();

            SetPopulation(populationGenes);

            RunSimulation();

            Debug.Log("Waiting until simulation finished... GENERATION: " + generationNumber);
            yield return new WaitUntil(() => runningCars == 0);

            var populationFitness = PopulationFitness();
            geneticAlgorithm.NextGeneration(populationFitness);

            RemovePopulation();
        }
        stopwatch.Stop();
        Debug.Log("SIMULATION FINISHED IN " + stopwatch.ElapsedMilliseconds / 1000d + "s");
        simulationFinished = true;
    }

    private double[] PopulationFitness()
    {
        double[] fitness = new double[population.Count];

        for (int i = 0; i < population.Count; i++)
        {
            fitness[i] = CarFitness(population[i]);
        }

        return fitness;
    }

    private GameObject BestCar()
    {
        double bestFitness = double.NegativeInfinity;
        GameObject bestCar = null;

        foreach (var car in activeCars)
        {
            var carFitness = CarFitness(car);
            if (carFitness > bestFitness)
            {
                bestCar = car;
                bestFitness = carFitness;
            }
        }

        return bestCar;
    }

    private void OnCarStopped(GameObject car)
    {
        if (!car.activeSelf) return;
        
        carsAIControls[car].enabled = false;
        car.SetActive(false);
        
        activeCars.Remove(car);
        runningCars--;
    }

    private void SetLayerSizes()
    {
        layerSizes = new int[hiddenLayersSizes.Length + 2]; // 2 is for input and output
        layerSizes[0] = inputLayerSize;

        var i = 0;
        for (; i < hiddenLayersSizes.Length; i++)
        {
            layerSizes[i + 1] = hiddenLayersSizes[i];
        }

        layerSizes[++i] = outputLayerSize;
    }

    private int CalculateNumberOfParameters(IReadOnlyList<int> layerSizes)
    {
        var numberOfParameters = 0;
        for (var i = 0; i < layerSizes.Count - 1; i++)
        {
            numberOfParameters += (layerSizes[i] + 1) * layerSizes[i + 1];
        }

        return numberOfParameters;
    }

    private void SetPopulation(double[][] populationGenes)
    {
        Debug.Assert(numberOfCars == populationGenes.Length);

        for (var i = 0; i < numberOfCars; i++)
        {
            var specimenGenes = populationGenes[i];
            var carWeights = GeneToWeights(specimenGenes);
            
            var car = carsPool.Dequeue();
            SetCar(car, carWeights);
            carsPool.Enqueue(car);

            population.Add(car);
            car.SetActive(true);
        }

        RemoveCollisionsBetweenCars();
    }

    private void RemovePopulation()
    {
        foreach (var car in activeCars)
        {
            car.SetActive(false);
            carsAIControls[car].enabled = false;
        }
        
        activeCars.Clear();
        population.Clear();
    }

    private void SetCar(GameObject car, double[][,] weights)
    {
        var driver = new NeuralNetwork(layerSizes, weights);

        var o = gameObject;
        car.transform.position = o.transform.position;
        car.transform.rotation = o.transform.rotation;

        carsAIControls[car].driver = driver;
        carsAIControls[car].AddStopEventListener(OnCarStopped);
        car.GetComponentInChildren<Radar>().numberOfRays = numberOfRays;
    }

    private void RemoveCollisionsBetweenCars()
    {
        var carsLayer = LayerMask.NameToLayer("Cars");

        foreach (var car in population)
        {
            car.layer = carsLayer;
        }
    }

    private double[][,] GeneToWeights(double[] genes)
    {
        var weights = new double[layerSizes.Length - 1][,];

        var currentGene = 0;
        for (var i = 0; i < layerSizes.Length - 1; i++)
        {
            var numberOfWeights = (layerSizes[i] + 1) * layerSizes[i + 1];
            var currentLayerWeights = new double[numberOfWeights];
            for (var j = 0; j < numberOfWeights; j++)
            {
                currentLayerWeights[j] = genes[currentGene++];
            }

            weights[i] = Make2DArray(currentLayerWeights, layerSizes[i] + 1, layerSizes[i + 1]);
        }

        return weights;
    }

    private double[] WeightsToGene(double[][,] weights)
    {
        var currentGene = 0;
        var genes = new double[numberOfGenes];
        foreach (var weightLayer in weights)
        {
            var flattened = Flatten(weightLayer);
            foreach (var weight in flattened)
            {
                genes[currentGene++] = weight;
            }
        }

        return genes;
    }

    private void RunSimulation()
    {
        foreach (var car in population)
        {
            activeCars.Add(car);
            carsAIControls[car].enabled = true;
            runningCars++;
        }
    }

    private static T[] Flatten<T>(T[,] input)
    {
        var height = input.GetLength(0);
        var width = input.GetLength(1);

        var output = new T[input.Length];
        for (var i = 0; i < height; i++)
        {
            for (var j = 0; j < width; j++)
            {
                output[i * width + j] = input[i, j];
            }
        }

        return output;
    }

    private static T[,] Make2DArray<T>(T[] input, int height, int width)
    {
        var output = new T[height, width];
        for (var i = 0; i < height; i++)
        {
            for (var j = 0; j < width; j++)
            {
                output[i, j] = input[i * width + j];
            }
        }

        return output;
    }

    private double CarFitness(GameObject car)
    {
        var carAI = carsAIControls[car];
        return distanceFactor * carAI.totalDistance + speedFactor * carAI.meanSpeed;
    }

    private void UpdateStatistics()
    {
        if(!bestCar) return;

        var carFitness = CarFitness(bestCar); 
        
        if (carFitness > MaxFitness)
        {
            var carAI = carsAIControls[bestCar];

            MaxSpeed = carAI.meanSpeed;
            TopDistance = carAI.totalDistance;
            MaxFitness = carFitness;
        }
    }
    
    private void SetDisplayInfo()
    {
        if(!bestCar) return;
        
        var carAI = carsAIControls[bestCar];

        simulationInfo.text = "Generation: " + generationNumber + "\n"
                              + "Running cars: " + runningCars + "\n"
                              + "Top distance: " + Math.Round(carAI.totalDistance, 3) + "\n"
                              + "Mean speed: " + Math.Round(carAI.meanSpeed, 3) + "\n\n"
                              + "In general: " + "\n"
                              + "Top distance: " + Math.Round(TopDistance, 3) + "\n"
                              + "Max speed: " + Math.Round(MaxSpeed, 3) + "\n"
                              + "Max fitness: " + Math.Round(MaxFitness, 3);
    }

    private List<int> GetNeuronsBoundaries(bool leftWise = true)
    {
        var boundaries = new List<int>();
        var lastSplit = -1;
        
        for (var i = 0; i < layerSizes.Length-1; i++)
        {
            var outgoingNeurons = layerSizes[i] + 1;
            var neuronsInLayer  = layerSizes[i + 1];

            if (!leftWise)
                (outgoingNeurons, neuronsInLayer) = (neuronsInLayer, outgoingNeurons);
            
            while(neuronsInLayer-- > 0)
            {
                lastSplit += outgoingNeurons;
                boundaries.Add(lastSplit);
            }
        }

        return boundaries;
    }

    private GeneticAlgorithm GetGeneticAlgorithm()
    {
        ISelectionStrategy ss;
        ICrossoverStrategy cs;
        IMutationStrategy ms;
        
        if(selectionType == SelectionType.Tournament)
            ss = new TournamentSelectionStrategy(tournamentSize);
        else if(selectionType == SelectionType.UBest)
            ss = new UBestSelectionStrategy(uSize);
        else if(selectionType == SelectionType.RouletteWheel)
            ss = new RouletteWheelSelectionStrategy();
        else
            throw new Exception("Selection strategy algorithm has to be initialized!");
        
        if(crossoverType == CrossoverType.KPoint)
            cs = new KPointCrossoverStrategy(numberOfDivisionPoints);
        else if(crossoverType == CrossoverType.SinglePoint)
            cs = new SinglePointCrossoverStrategy();
        else if(crossoverType == CrossoverType.UniformPoint)
            cs = new UniformPointCrossoverStrategy();
        else if(crossoverType == CrossoverType.FixedPointByNeurons)
            cs = new FixedPointsCrossoverStrategy(GetNeuronsBoundaries());
        else
            throw new Exception("Crossover strategy algorithm has to be initialized!");
        
        if(mutationType == MutationType.Shrink)
            ms = new ShrinkMutationStrategy(shrinkMutationRate);
        else if(mutationType == MutationType.Uniform)
            ms = new UniformMutationStrategy(uniformMutationRate);
        else
            throw new Exception("Mutation strategy algorithm has to be initialized!");
        
        return new GeneticAlgorithm(numberOfCars, numberOfGenes, ss, cs, ms, crossoverProbability, mutationProbability, elitism);
    }
}
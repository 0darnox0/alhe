﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class Tester : MonoBehaviour
{
    public List<GeneticAlgorithmParams> testParams;
    private GeneticController geneticController;
    public string filePath = "test.csv";
    public int repeatTests = 3;

    // Start is called before the first frame update
    void Start()
    {
        geneticController = GameObject.Find("GeneticController").GetComponent<GeneticController>();
        geneticController.enabled = false;
        
        testParams = new List<GeneticAlgorithmParams>();

        for (int i = 0; i < repeatTests; i++)
        {
            testParams.AddRange(GenerateTestCase3());
            testParams.AddRange(GenerateTestCase4());
            testParams.AddRange(GenerateTestCase5());
        }

        StartCoroutine(TestingLoop());
    }

    private List<GeneticAlgorithmParams> GenerateTestCase1()
    {
        var tests = new List<GeneticAlgorithmParams>();
        
        var selectionTypes =
            (GeneticController.SelectionType[]) Enum.GetValues(typeof(GeneticController.SelectionType));
        var crossoverTypes =
            (GeneticController.CrossoverType[]) Enum.GetValues(typeof(GeneticController.CrossoverType));
        var mutationTypes = 
            (GeneticController.MutationType[]) Enum.GetValues(typeof(GeneticController.MutationType));

        var enumerable = crossoverTypes.ToList();
        foreach (var selectionType in selectionTypes)
        {
            foreach (var crossoverType in enumerable)
            {
                foreach (var mutationType in mutationTypes)
                {
                    var param = new GeneticAlgorithmParams
                    {
                        selectionType = selectionType, 
                        crossoverType = crossoverType, 
                        mutationType = mutationType
                    };
                        
                    tests.Add(param);
                }
            }
        }
        
        return tests;
    }

    private List<GeneticAlgorithmParams> GenerateTestCase2()
    {
        var tests = GenerateTestCase1();
        
        foreach (var test in tests)
        {
            test.mutationProbability = 0.05;
            test.uniformMutationRate = 140;
            test.shrinkMutationRate = 140;
        }

        return tests;
    }
    
    private List<GeneticAlgorithmParams> GenerateTestCase3()
    {
        var tests = GenerateTestCase2();
        
        foreach (var test in tests)
        {
            test.elitism = false;
        }

        return tests;
    }
    
    private List<GeneticAlgorithmParams> GenerateTestCase4()
    {
        var tests = GenerateTestCase1();
        
        foreach (var test in tests)
        {
            test.mutationProbability = 0.1;
        }

        return tests;
    }
    
    private List<GeneticAlgorithmParams> GenerateTestCase5()
    {
        var tests = GenerateTestCase1();
        
        foreach (var test in tests)
        {
            test.uniformMutationRate = 280;
            test.shrinkMutationRate = 280;
        }

        return tests;
    }
    
    private IEnumerator TestingLoop()
    {
        using (var file = new StreamWriter(filePath, true))
        {
            var csv = new StringBuilder();
            AddHeaderToCsv(csv);
            
            file.Write(csv.ToString());
            file.Flush();
                
            csv.Clear();
            
            foreach (var param in testParams)
            {
                geneticController.enabled = false;

                SetGeneticController(param);
                AddParamsToCsv(param, csv);
                
                Debug.Log("PARAMS: " + csv.ToString());
                
                geneticController.enabled = true;
                yield return new WaitUntil(() => geneticController.simulationFinished);

                var topDistance = geneticController.TopDistance;
                var maxSpeed = geneticController.MaxSpeed;
                var generationNumber = geneticController.generationNumber;

                csv.Append(topDistance + ";");
                csv.Append(maxSpeed + ";");
                csv.Append(generationNumber + ";");

                csv.Append("\n");

                file.Write(csv.ToString());
                file.Flush();
                
                csv.Clear();
            }
        }
    }

    private void AddHeaderToCsv(StringBuilder csv)
    {
        csv.Append("selectionType" + ";");
        csv.Append("crossoverType" + ";");
        csv.Append("mutationType" + ";");
        csv.Append("crossoverProbability" + ";");
        csv.Append("mutationProbability" + ";");
        csv.Append("elitism" + ";");
        csv.Append("distanceFactor" + ";");
        csv.Append("speedFactor" + ";");
        csv.Append("tournamentSize" + ";");
        csv.Append("uSize" + ";");
        csv.Append("numberOfDivisionPoints" + ";");
        csv.Append("shrinkMutationRate" + ";");
        csv.Append("uniformMutationRate" + ";");
        csv.Append("topDistance" + ";");
        csv.Append("maxSpeed" + ";");
        csv.Append("generationNumber" + ";");

        csv.Append("\n");
    }

    private void AddParamsToCsv(GeneticAlgorithmParams param, StringBuilder csv)
    {
        csv.Append(param.selectionType + ";");
        csv.Append(param.crossoverType + ";");
        csv.Append(param.mutationType + ";");
        csv.Append(param.crossoverProbability + ";");
        csv.Append(param.mutationProbability + ";");
        csv.Append(param.elitism + ";");
        csv.Append(param.distanceFactor + ";");
        csv.Append(param.speedFactor + ";");
        csv.Append(param.tournamentSize + ";");
        csv.Append(param.uSize + ";");
        csv.Append(param.numberOfDivisionPoints + ";");
        csv.Append(param.shrinkMutationRate + ";");
        csv.Append(param.uniformMutationRate + ";");
    }

    private void SetGeneticController(GeneticAlgorithmParams param)
    {
        geneticController.selectionType = param.selectionType;
        geneticController.crossoverType = param.crossoverType;
        geneticController.mutationType = param.mutationType;
        geneticController.crossoverProbability = param.crossoverProbability;
        geneticController.mutationProbability = param.mutationProbability;
        geneticController.elitism = param.elitism;
        geneticController.distanceFactor = param.distanceFactor;
        geneticController.speedFactor = param.speedFactor;
        geneticController.tournamentSize = param.tournamentSize;
        geneticController.uSize = param.uSize;
        geneticController.numberOfDivisionPoints = param.numberOfDivisionPoints;
        geneticController.shrinkMutationRate = param.shrinkMutationRate;
        geneticController.uniformMutationRate = param.uniformMutationRate;
    }
}

public class GeneticAlgorithmParams
{
    public GeneticController.SelectionType selectionType = GeneticController.SelectionType.RouletteWheel;
    public GeneticController.CrossoverType crossoverType = GeneticController.CrossoverType.KPoint;
    public GeneticController.MutationType mutationType = GeneticController.MutationType.Uniform;
    public double crossoverProbability = 0.95;
    public double mutationProbability = 0.01;
    public bool elitism = true;
    public double distanceFactor = 1.0;
    public double speedFactor = 0.0;
    public int tournamentSize = 10;
    public int uSize = 10;
    public int numberOfDivisionPoints = 10;
    public int shrinkMutationRate = 5;
    public int uniformMutationRate = 5;
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class CameraFollower : FreeLookCam
{    
    private GeneticController geneticController;

    protected override void Start()
    {
        geneticController = GameObject.Find("GeneticController").GetComponent<GeneticController>();
        
        base.Start();
    }
    
    public override void FindAndTargetPlayer()
    {
        var targetObj = geneticController.bestCar;
        if (targetObj)
        {
            SetTarget(targetObj.transform);
        }
    }
}

﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]
    public class CarAIControl : MonoBehaviour
    {
        public NeuralNetwork driver;

        private CarController carController; // the car controller we want to use
        public Radar radar;

        private double steer = 0f;
        private double accel = 0f;

        private Vector3 lastPosition;
        private double totalTime = 0f;

        public double totalDistance { get; private set; } = 0.0f;
        public double meanSpeed => totalTime != 0f ? totalDistance / totalTime : 0f;

        private CarStoppedEvent carStoppedEvent;
        private bool slowCarTestPerformed;

        private void Awake()
        {
            // get the car controller
            carController = GetComponent<CarController>();
            radar = GetComponentInChildren<Radar>();
            carStoppedEvent = new CarStoppedEvent();

            Debug.Assert(radar != null);
        }

        private void OnEnable()
        {
            lastPosition = transform.position;
            slowCarTestPerformed = false;
            totalTime = 0;
            totalDistance = 0;
        }

        public void AddStopEventListener(UnityAction<GameObject> call)
        {
            carStoppedEvent.RemoveAllListeners();
            carStoppedEvent.AddListener(call);
        }
        
        void OnCollisionEnter(Collision collision)
        {
            if (isCollidingWithScreen(collision))
            {
                TriggerCarStop();
            }
        }

        public void TriggerCarStop()
        {
            carStoppedEvent.Invoke(gameObject);
        }

        void StopCarIfTooSlow()
        {
            if (meanSpeed < 0.2)
            {
                TriggerCarStop();
            }

            slowCarTestPerformed = true;
        }

        private bool isCollidingWithScreen(Collision collision)
        {
            return collision.gameObject.name == "Left_screen" || collision.gameObject.name == "Right_screen";
        }

        private void FixedUpdate()
        {
            var position = transform.position;
            var distance = Vector3.Distance(position, lastPosition);

            totalTime += Time.deltaTime;
            totalDistance += distance;

            lastPosition = position;

            var radarDistances = radar.distances;
            var neuralInput = new double[radarDistances.Length];

            for (var i = 0; i < radarDistances.Length; i++)
            {
                neuralInput[i] = radarDistances[i];
            }

            var steerings = driver.Predict(neuralInput);
            steer = steerings[0] * 2 - 1; // convert to range [-1, 1]
            accel = steerings[1];

            carController.Move((float) steer, (float) accel, 0.0f, 0.0f);

            if (!slowCarTestPerformed && totalTime > 3) StopCarIfTooSlow();
        }
        
        private class CarStoppedEvent : UnityEvent<GameObject>
        {
        }
    }
}
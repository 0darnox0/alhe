﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Radar : MonoBehaviour
{

    public float laserRange = 50f;
    public float fieldOfView = 180f;
    public int numberOfRays = 3;
    public float raySize = 0.0025f;
    public Material rayMaterial;

    private List<LineRenderer> laserRaysRenderers = new List<LineRenderer>();
    private int carsLayerMask;
    
    public double[] distances { get; private set; }

    void Start()
    {
        for (int i = 0; i < numberOfRays; i++)
        {
            GameObject ray = new GameObject();

            LineRenderer lineRendered = ray.AddComponent<LineRenderer>();
            lineRendered.startWidth = (float) raySize;
            lineRendered.endWidth = (float) raySize;
            lineRendered.material = rayMaterial;

            ray.transform.SetParent(gameObject.transform);

            laserRaysRenderers.Add(ray.GetComponent<LineRenderer>());
        }
        
        carsLayerMask = LayerMask.GetMask("Cars");
        carsLayerMask = ~carsLayerMask;
    }

    private void OnEnable()
    {
        distances = new double[numberOfRays];
    }

    void FixedUpdate()
    {
        Vector3 rayOrigin = this.transform.position;
        Vector3 radarForward = this.transform.forward;

        float shift = fieldOfView / (numberOfRays + 1);

        for (int i = 0; i < laserRaysRenderers.Count; i++)
        {
            var line = laserRaysRenderers[i];

            RaycastHit hit;

            line.SetPosition(0, rayOrigin);

            float rayAngle = -fieldOfView / 2 + shift * (i + 1);
            Vector3 direction = Quaternion.AngleAxis(rayAngle, Vector3.up) * radarForward;

            if (Physics.Raycast(rayOrigin, direction, out hit, laserRange, layerMask:carsLayerMask))
            {
                line.SetPosition(1, hit.point);
                distances[i] = hit.distance;
            }
            else
            {
                line.SetPosition(1, rayOrigin + (direction * laserRange));
                distances[i] = laserRange;
            }
        }

    }

}
using System;
using System.Collections.Generic;
using System.Linq;

public class GeneticAlgorithm
{
    private int populationSize;
    private int numberOfGenes;
    private IList<Specimen> population;

    private double crossoverProbability;
    private double mutationProbability;
    private bool elitism;

    ISelectionStrategy selectionStrategy;
    ICrossoverStrategy crossoverStrategy;
    IMutationStrategy mutationStrategy;

    public GeneticAlgorithm(
        int populationSize,
        int numberOfGenes,
        ISelectionStrategy selectionStrategy,
        ICrossoverStrategy crossoverStrategy,
        IMutationStrategy mutationStrategy,
        double crossoverProbability = 0.95,
        double mutationProbability = 0.01,
        bool elitism = true
    )
    {
        this.populationSize = populationSize;
        this.numberOfGenes = numberOfGenes;

        this.selectionStrategy = selectionStrategy;
        this.crossoverStrategy = crossoverStrategy;
        this.mutationStrategy = mutationStrategy;

        this.crossoverProbability = crossoverProbability;
        this.mutationProbability = mutationProbability;
        this.elitism = elitism;

        DrawPopulation();
    }

    private void DrawPopulation()
    {
        population = new List<Specimen>();

        for (var i = 0; i < populationSize; i++)
        {
            Specimen specimen = new Specimen(numberOfGenes);
            population.Add(specimen);
        }
    }

    public double[][] GetPopulation()
    {
        double[][] allSpecimensGenes = new double[populationSize][];

        for (int i = 0; i < populationSize; i++)
        {
            allSpecimensGenes[i] = population[i].genes;
        }

        return allSpecimensGenes;
    }

    public void NextGeneration(double[] objectiveFunctions)
    {
        if (objectiveFunctions.Length != populationSize)
            throw new Exception("Number of passed objective functions doesn't match with population size.");

        SetObjectiveFunctions(objectiveFunctions);

        // Find the best specimen of current population
        var maxObjective = population.Max(s => s.objectiveFunction);
        var bestSpecimen = population.First(x => x.objectiveFunction == maxObjective);

        population = Mutation(Crossover(Selection(population)));

        if (elitism)
        {
            // Check if the best specimen isn't already in the new population
            if (!population.Any(x => x.genes == bestSpecimen.genes))
            {
                // Replace random specimen of the new population with the best one of previous
                var replacementIndex = Random.Next(0, population.Count);
                population[replacementIndex] = bestSpecimen;
            }
        }
    }

    private void SetObjectiveFunctions(double[] objectiveFunctions)
    {
        for (int i = 0; i < populationSize; i++)
        {
            population[i].objectiveFunction = objectiveFunctions[i];
        }
    }

    private IEnumerator<Specimen> Selection(IList<Specimen> previousPopulation)
    {
        var parentsGenerator = selectionStrategy.Selection(previousPopulation).GetEnumerator();
        return parentsGenerator;
    }

    private IList<Specimen> Crossover(IEnumerator<Specimen> parentsGenerator)
    {
        var offspring = new List<Specimen>();

        for (var i = 0; i < populationSize; i++)
        {
            parentsGenerator.MoveNext();
            var parent1 = parentsGenerator.Current;

            Specimen children;
            if (Random.NextDouble() < crossoverProbability)
            {
                parentsGenerator.MoveNext();
                var parent2 = parentsGenerator.Current;

                children = crossoverStrategy.Crossover(parent1, parent2);
            }
            else
            {
                children = parent1;
            }

            offspring.Add(children);
        }

        parentsGenerator.Dispose();
        return offspring;
    }

    private IList<Specimen> Mutation(IList<Specimen> embryos)
    {
        var newPopulation = new List<Specimen>();

        foreach (var embryo in embryos)
        {
            Specimen newSpecimen;
            if (Random.NextDouble() < mutationProbability)
            {
                newSpecimen = mutationStrategy.Mutation(embryo);
            }
            else
            {
                newSpecimen = embryo;
            }

            newPopulation.Add(newSpecimen);
        }

        return newPopulation;
    }
}

public class Specimen
{
    public double[] genes { set; get; }
    public double objectiveFunction;

    private double[] DrawGenotype(int numberOfGenes)
    {
        double[] genotype = new double[numberOfGenes];

        for (int i = 0; i < numberOfGenes; i++)
        {
            genotype[i] = Random.NextDouble(-1.0, 1.0);
        }

        return genotype;
    }

    public Specimen(int numberOfGenes, bool drawGenotype = true)
    {
        if (drawGenotype)
        {
            genes = DrawGenotype(numberOfGenes);
        }
        else
        {
            genes = new double[numberOfGenes];
        }
    }
}

public interface ISelectionStrategy
{
    IEnumerable<Specimen> Selection(IList<Specimen> population);
}

public interface ICrossoverStrategy
{
    Specimen Crossover(Specimen parent1, Specimen parent2);
}

public interface IMutationStrategy
{
    Specimen Mutation(Specimen embryo);
}

class UBestSelectionStrategy : ISelectionStrategy
{
    private int u;

    public UBestSelectionStrategy(int u)
    {
        this.u = u;
    }

    public IEnumerable<Specimen> Selection(IList<Specimen> population)
    {
        population = population.OrderByDescending(s => s.objectiveFunction).Take(u).ToList();

        while (true)
        {
            var selectedIndex = Random.Next(0, population.Count);
            yield return population[selectedIndex];
        }
    }
}

class RouletteWheelSelectionStrategy : ISelectionStrategy
{
    public IEnumerable<Specimen> Selection(IList<Specimen> population)
    {
        double totalSum = population.Sum(s => s.objectiveFunction);
        population = population.OrderByDescending(s => s.objectiveFunction).ToList();

        while (true)
        {
            double prefixSum = 0;
            double selectionPoint = Random.NextDouble();

            var selectedSpecimen = population.First(x =>
            {
                prefixSum += x.objectiveFunction / totalSum;
                return selectionPoint < prefixSum;
            });

            yield return selectedSpecimen;
        }
    }
}

class TournamentSelectionStrategy : ISelectionStrategy
{
    private int m_TournamentSize;

    public TournamentSelectionStrategy(int tournamentSize)
    {
        m_TournamentSize = tournamentSize;
    }

    public IEnumerable<Specimen> Selection(IList<Specimen> population)
    {
        while (true)
        {
            // Select random specimens to the tournament
            var tournament = new List<Specimen>();
            for (int i = 0; i < m_TournamentSize; i++)
            {
                var selectedIndex = Random.Next(0, population.Count);
                tournament.Add(population[selectedIndex]);
            }

            // Choose the best one
            var maxObjective = tournament.Max(s => s.objectiveFunction);
            var selected = tournament.First(x => x.objectiveFunction == maxObjective);

            yield return selected;
        }
    }
}

abstract class PointsCrossoverStrategy : ICrossoverStrategy
{
    public abstract Specimen Crossover(Specimen parent1, Specimen parent2);

    protected static Specimen PointsCrossover(Specimen parent1, Specimen parent2, List<int> splitPoints)
    {
        if (parent1.genes.Length != parent2.genes.Length)
            throw new Exception("Parents must have the same number of genes");

        var numberOfGenes = parent1.genes.Length;
        var children = new Specimen(numberOfGenes, false);

        splitPoints.Sort();
        var currentSplitPoint = 0;
        for (var i = 0; i < numberOfGenes; i++)
        {
            children.genes[i] = parent1.genes[i];

            if (currentSplitPoint < splitPoints.Count && splitPoints[currentSplitPoint] == i)
            {
                (parent1, parent2) = (parent2, parent1); // Swap parents
                currentSplitPoint++;
            }
        }

        return children;
    }
}

class FixedPointsCrossoverStrategy : PointsCrossoverStrategy
{
    private List<int> splitPoints;

    public FixedPointsCrossoverStrategy(List<int> splitPoints)
    {
        foreach (var splitPoint in splitPoints)
            if (splitPoint < 0)
                throw new Exception("Split points must be greater than 0.");

        this.splitPoints = splitPoints;
    }

    public override Specimen Crossover(Specimen parent1, Specimen parent2)
    {
        var children = PointsCrossover(parent1, parent2, splitPoints);

        return children;
    }
}

class KPointCrossoverStrategy : PointsCrossoverStrategy
{
    private int kPoint;

    public KPointCrossoverStrategy(int kPoint)
    {
        this.kPoint = kPoint;
    }

    public override Specimen Crossover(Specimen parent1, Specimen parent2)
    {
        if (kPoint < 0) throw new Exception("Number of k-points cannot be lower than 0");
        if (kPoint >= parent1.genes.Length)
            throw new Exception("Number of k-points must be lower than number of genes.");

        var numberOfGenes = parent1.genes.Length;
        var splitPoints = Enumerable.Range(0, numberOfGenes).OrderBy(x => Random.Next()).Take(kPoint).ToList();

        var children = PointsCrossover(parent1, parent2, splitPoints);

        return children;
    }
}

class SinglePointCrossoverStrategy : KPointCrossoverStrategy
{
    public SinglePointCrossoverStrategy() : base(1)
    {
    }
}

class UniformPointCrossoverStrategy : ICrossoverStrategy
{
    public Specimen Crossover(Specimen parent1, Specimen parent2)
    {
        if (parent1.genes.Length != parent2.genes.Length)
            throw new Exception("Parents must have the same number of genes");

        var numberOfGenes = parent1.genes.Length;

        var children = new Specimen(numberOfGenes, false);

        for (var i = 0; i < numberOfGenes; i++)
        {
            children.genes[i] = parent1.genes[i];

            if (Random.NextDouble() < 0.5f)
            {
                (parent1, parent2) = (parent2, parent1); // Swap parents
            }
        }

        return children;
    }
}

class UniformMutationStrategy : IMutationStrategy
{
    private int mutationRate;

    public UniformMutationStrategy(int mutationRate)
    {
        this.mutationRate = mutationRate;
    }

    public Specimen Mutation(Specimen embryo)
    {
        var numberOfGenes = embryo.genes.Length;
        var mutationProb = mutationRate / (double)numberOfGenes;

        for (var j = 0; j < numberOfGenes; j++)
        {
            if (Random.NextDouble() < mutationProb)
                embryo.genes[j] = Random.NextDouble(-1.0, 1.0);
        }

        return embryo;
    }
}

class ShrinkMutationStrategy : IMutationStrategy
{
    private int mutationRate;

    public ShrinkMutationStrategy(int mutationRate)
    {
        this.mutationRate = mutationRate;
    }

    public Specimen Mutation(Specimen embryo)
    {
        var numberOfGenes = embryo.genes.Length;
        var mutationProb = mutationRate / (double)numberOfGenes;

        for (var j = 0; j < numberOfGenes; j++)
        {
            if (Random.NextDouble() < mutationProb)
            {
                var newGene = Random.NextGaussian(embryo.genes[j], 0.5);
                newGene = Math.Max(Math.Min(newGene, 1), -1); // Clamp gene to [-1, 1]
                embryo.genes[j] = newGene;
            }
        }

        return embryo;
    }
}
/*
    The neural network class
    Implements methods for simple feed forward neural network
*/

using System;
using System.Linq;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;

public class NeuralNetwork
{
    private Matrix<double>[] weights;
    private int hiddenLayersNum;
    private int layersNum;

    /// layerSizes - defines size of each layer and number of layers at all. 
    /// The first one is an input layer and the last one is reserved for an output. 
    /// All intermediate layers are so called hidden layers (at least one is required).
    /// weights - defines weights matrix for each layer (except the last one).
    public NeuralNetwork(int[] layerSizes, double[][,] weights)
    {
        Control.UseManaged();
        
        if (layerSizes.Length < 3)
        {
            throw new Exception("Minimal number of layers is 3 (input, at least one hidden, output).");
        }

        if (!areSizesPositive(layerSizes))
        {
            throw new Exception("All sizes need to be positive numbers.");
        }

        checkDimensions(layerSizes, weights);

        layersNum = layerSizes.Length;
        hiddenLayersNum = layersNum - 2;

        this.weights = new Matrix<double>[layersNum - 1];

        for (var i = 0; i < layersNum - 1; ++i)
        {
            this.weights[i] = Matrix<double>.Build.DenseOfArray(weights[i]).Transpose();
        }
    }

    /// Given input vector (of first layer's size) performs forward propagation 
    /// over all layers and returns vector of calculated results.
    public double[] Predict(double[] input)
    {
        var layerOutput = Vector<double>.Build.Dense(input);

        Vector<double> layerInput;
        for (var i = 0; i < hiddenLayersNum; ++i)
        {
            layerInput = CreateVector.DenseOfEnumerable(layerOutput.Append(1.0d));
            var layerWeights = weights[i];
            layerOutput = Sigmoid(layerWeights * layerInput);
        }
        
        layerInput = CreateVector.DenseOfEnumerable(layerOutput.Append(1.0d));
        layerOutput = weights[layersNum - 2] * layerInput;
        
        return Softmax(layerOutput).ToArray();
    }

    // Hidden layers' activation function 
    private Vector<double> Sigmoid(Vector<double> input)
    {
        for (var i = 0; i < input.Count; ++i)
        {
            input[i] = 1 / (1 + Math.Exp(input[i]));
        }

        return input;
    }

    // Output layer's activation function
    private Vector<double> Softmax(Vector<double> input)
    {
        double sum = 0;
        for (int i = 0; i < input.Count; i++)
        {
            input[i] = Math.Exp(input[i]);
            sum += input[i];
        }

        for (int i = 0; i < input.Count; ++i)
        {
            input[i] = input[i] / sum;
        }

        return input;
    }

    private bool areSizesPositive(int[] layerSizes)
    {
        for (int i = 0; i < layerSizes.Length; ++i)
        {
            if (layerSizes[i] <= 0)
                return false;
        }

        return true;
    }

    private void checkDimensions(int[] layerSizes, double[][,] weights)
    {
        if (weights.Length != layerSizes.Length - 1)
        {
            throw new Exception("Number of layers is not compatible with provided number of weights.");
        }

        for (int i = 0; i < weights.Length; ++i)
        {
            if (weights[i].GetLength(0) != layerSizes[i] + 1)
            {
                throw new Exception("When multiplying matrices and vectors width of weights at " + i +
                                    " layer is incorrect.");
            }

            if (weights[i].GetLength(1) != layerSizes[i + 1])
            {
                throw new Exception("When multiplying matrices and vectors height of weights at " + i +
                                    " layer is incorrect.");
            }
        }
    }

    // For testing purposes
    public void PrintWeights()
    {
        Console.WriteLine("Weights: ");
        for (int i = 0; i < layersNum - 1; i++)
        {
            Console.Write(weights[i].ToString());
        }
    }
}
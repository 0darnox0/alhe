using System;

public static class Random
{
    private static readonly int seed;
    private static readonly System.Random random;
    private static readonly object randomLock = new object();

    static Random()
    {
        seed = Environment.TickCount;
        random = new System.Random(seed);
    }
    
    public static int Next()
    {
        lock (randomLock)
        {
            return random.Next();
        }
    }
    
    public static int Next(int min, int max)
    {
        lock (randomLock)
        {
            return random.Next(min, max);
        }
    }

    public static double NextDouble()
    {
        lock (randomLock)
        {
            return random.NextDouble();
        }
    }

    public static double NextDouble(double min, double max)
    {
        double randomDouble;

        lock (randomLock)
        {
            randomDouble = random.NextDouble();
        }

        return randomDouble * (max - min) + min;
    }

    public static double NextGaussian(double mean, double stdDev)
    {
        double u1;
        double u2;

        lock (randomLock)
        {
            u1 = 1.0 - random.NextDouble();
            u2 = 1.0 - random.NextDouble();
        }

        var randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);

        return mean + stdDev * randStdNormal;
    }
}